import countChange from './cartEvents/productCountChange';
import productUpdate from './cartEvents/productInfoUpdate';
import inputOnFocus from './formEvents/inputOnFocus';
import customSelect from './formEvents/customSelect';
import selectClickOut from './formEvents/selectClickOut';
import inputAddType from './formEvents/emailOrPhoneInput';
import shippingInfo from './checkoutEvents/shippingInfo';
import paymentInfo from './checkoutEvents/paymentInfo';
import checkedMethod from './checkoutEvents/checkedMethod';
import changeRadioButton from './checkoutEvents/changeRadioButton';
import './cartEvents/checkoutCart';

const countChangeElements = document.querySelectorAll('.productCard__quantityItem');
if(countChangeElements) {
  countChangeElements.forEach(productAddElement => {
    productAddElement.addEventListener('click', countChange)
  });
}

const productUpdateButtons = document.querySelectorAll('.productCard__count .button--success');
productUpdateButtons.forEach(productUpdateButton => {
  productUpdateButton.addEventListener('click', productUpdate)
});

const currentInputs = document.querySelectorAll('.customInput__input');
currentInputs.forEach(currentInput => {
  currentInput.addEventListener('blur', inputOnFocus)
});

const selectElements = document.querySelectorAll(".customSelect__currentOption");
if(selectElements) {
  selectElements.forEach(selectElement => {
    selectElement.addEventListener("click", customSelect);
  });
}

const checkoutElement = document.querySelector(".checkout");
if(checkoutElement) {
  checkoutElement.addEventListener("click", selectClickOut);
}

const mailOrPhoneInput = document.querySelector('.contactInfoForm__phoneOrMailInput label .customInput__input');
if(mailOrPhoneInput) {
  mailOrPhoneInput.addEventListener('input', inputAddType);
}

shippingInfo();
paymentInfo();

const shipToElement = document.querySelector('#shipToInfo .customerInfo__value');
if (shipToElement) {
  shipToElement.innerHTML = localStorage.getItem('country')
}

const contactInfoElement = document.querySelector('#contactInfo .customerInfo__value');
if (contactInfoElement) {
  contactInfoElement.innerHTML = `+${localStorage.getItem('phoneOrMail')}`
}

const paymentMethodElements = document.querySelectorAll('input[name=shippingMethod]');
if (paymentMethodElements) {
  paymentMethodElements.forEach (paymentMethodElement => {
    paymentMethodElement.addEventListener('change', checkedMethod)
  })
}

const billingAddressElements = document.querySelectorAll('input[name=billingAddress]');

if (billingAddressElements) {
  billingAddressElements.forEach(billingAddressElement => {
    billingAddressElement.addEventListener('change', changeRadioButton);
  })
}
