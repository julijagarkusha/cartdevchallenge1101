const productUpdate = (event) => {
  event.preventDefault();
  const target = event.target;

  const productQuantityElem = target.parentElement.querySelector('.productCard__quantity--switch').value;
  let totalPriceElem = target.parentElement.querySelector('.productCard__price--total');
  const productPrice = target.parentElement.querySelector('.productCard__price').innerHTML;

  const priceToNumber = parseInt(productPrice.replace(/[^.\d]+/g,"").replace( /^([^\.]*\.)|\./g, '$1' ));
  const totalPrice = parseInt(productQuantityElem) * priceToNumber;
  totalPriceElem.innerHTML = `$${totalPrice}.00`;
  localStorage.setItem(`${target.parentNode.parentNode.id}Total`, totalPrice);
  localStorage.setItem(`${target.parentNode.parentNode.id}Quantity`, productQuantityElem);

  productTotal();
};

const productTotal = () => {
  let sumPrice = 0;
  let sumWeight = 0;

  let totalPriceElements = document.querySelectorAll(".productCard__price--total");
  let productCards = document.querySelectorAll(".productCard");

  totalPriceElements.forEach(totalPriceElement => {
    sumPrice += parseFloat(totalPriceElement.innerHTML.slice(1));
  });

  productCards.forEach(productCard => {
    const productItemWeight = productCard.querySelector('.productWeight');
    const productItemCount = productCard.querySelector('.productCard__quantity--switch');
    const totalItemWeightValue = Number(productItemCount.value)*Number(productItemWeight.innerHTML.slice(0, length-2));
    sumWeight += Number(totalItemWeightValue.toFixed(1));
  });

  localStorage.setItem('totalPrice', sumPrice);
  localStorage.setItem('totalWeight', sumWeight.toFixed(2));

  document.querySelector('#totalPrice').innerHTML = `$${sumPrice}.00`;
  document.querySelector('#totalWeight').innerHTML = `${sumWeight.toFixed(2)}kg`
};

export default productUpdate;
