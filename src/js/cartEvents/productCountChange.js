const countChange = (event) => {
  event.preventDefault();
  const target = event.target;
  const targetClassList = target.classList.value;
  if (targetClassList.indexOf('productCard__quantity--add') !== (-1)) {
    let countElement =  target.previousElementSibling;
    countElement.value = parseInt(target.previousElementSibling.value) + 1;
  } else {
    let countElement =  target.nextElementSibling;
    let countValue = parseInt(target.nextElementSibling.value);
    countElement.value = countValue <= 0 ? '0' : countValue - 1;
  }
};

export default countChange;
