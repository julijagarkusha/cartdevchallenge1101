const quantityElements = document.querySelectorAll('.productCard__quantity--switch');
if(quantityElements) {
  quantityElements.forEach(quantityElement => {
    const productCard = quantityElement.parentNode.parentNode.parentNode.parentNode;
    const localStorageValue = `${productCard.id}Quantity`;
    if (localStorage.getItem(localStorageValue) !== null) {
      quantityElement.value = localStorage.getItem(localStorageValue)
    } else {
      localStorage.setItem(localStorageValue, quantityElement.value)
    }
  });
}

const priceElements = document.querySelectorAll('.productCard__price--total');
if (priceElements) {
  priceElements.forEach(priceElement => {
    const productCard = priceElement.parentNode.parentNode.parentNode;
    const localStorageValue = `${productCard.id}Total`;
    if (localStorage.getItem(localStorageValue) !== null) {
      priceElement.innerHTML = `$${localStorage.getItem(localStorageValue)}.00`
    }
  });
}

const totalPrice = document.getElementById('totalPrice');
if (totalPrice) {
  const totalPriceValue = Number(totalPrice.innerHTML.substr(1));
  if (localStorage.getItem('totalPrice') !== null) {
    totalPrice.innerHTML = `$${localStorage.getItem('totalPrice')}.00`
  } else {
    localStorage.setItem('totalPrice', totalPriceValue);
  }
}

const totalWeight = document.getElementById('totalWeight');
if(totalWeight) {
  const totalWeightValue = Number(totalWeight.innerHTML.substring(0, totalWeight.innerHTML.length-2));
  if (localStorage.getItem('totalWeight') !== null) {
    totalWeight.innerHTML = `${localStorage.getItem('totalWeight')}kg`
  } else {
    localStorage.setItem('totalWeight', totalWeightValue);
  }
}
const subTotalPrice = document.getElementById('checkoutSubtotalPrice');
const totalPriceCheckout = document.getElementById('checkoutTotalPrice');
if (subTotalPrice) {
  subTotalPrice.innerHTML = `$${localStorage.getItem('totalPrice')}.00`;
}
if(totalPriceCheckout) {
  totalPriceCheckout.innerHTML = `$${localStorage.getItem('totalPrice')}.00`;
}

const products = document.querySelectorAll('.checkout__rightColumn .productCard');
products.forEach(product => {
  switch (product.id) {
    case 'product1' : product.querySelector('.productCard__quantity').innerHTML = localStorage.getItem('product1Quantity') || '1';break;
    case 'product2' : product.querySelector('.productCard__quantity').innerHTML = localStorage.getItem('product2Quantity') || '1';break;
    case 'product3' : product.querySelector('.productCard__quantity').innerHTML = localStorage.getItem('product3Quantity') || '1';break;
    case 'product4' : product.querySelector('.productCard__quantity').innerHTML = localStorage.getItem('product4Quantity') || '1';break;
  }
});
