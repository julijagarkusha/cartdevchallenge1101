const changeRadioButton = (event) => {
  const target = event.target;
  const billingInfoWrapper = target.parentNode.parentNode.parentNode.parentNode;
  const billingInfoElement =   billingInfoWrapper.querySelector('.billingInfo');
  if(target.value === 'evenShippingAddress') {
    billingInfoElement.classList.value = 'billingInfo'
  } else {
    billingInfoElement.classList.value = 'billingInfo billingInfo--visible'
  }
};

export default changeRadioButton;
