const shippingInfo = () => {
  const shippingInfoWrapper = document.querySelector('.shippingMethod');
  if (window.location.search !== '' && shippingInfoWrapper) {
    let customInfo = window.location.search.replace("?", "");
    let customInfoArray = customInfo.split('&');
    const formValues = {};
    customInfoArray.forEach(customInfoItem => {
      const itemsValue = customInfoItem.split('=');
      formValues[itemsValue[0]] = itemsValue[1];
    });

    let numberOrMail = formValues.EmailOrMobile.replace('%2B', '');
    console.log(numberOrMail);
    const customContact = document.querySelector('#contactInfo .customerInfo__value');

    const address = formValues.address;
    const apartment = formValues.apartment;
    const city = formValues.city;
    const country = formValues.country;
    if(country) {
      localStorage.setItem('country', country);
    }

    const customShip = document.querySelector('#shipToInfo .customerInfo__value');
    if (customShip) {
      const shipAddress = `${address} ${apartment} ${city} ${country}`;
      if (localStorage.getItem('shipAddress') !== null) {
        customShip.innerHTML = localStorage.getItem('shipAddress');
      } else {
        localStorage.setItem('shipAddress', shipAddress);
      }
    }
    if (customContact) {
      localStorage.setItem('phoneOrMail', numberOrMail);
      customContact.innerHTML = numberOrMail || localStorage.getItem('phoneOrMail');
    }
  }
};

export default shippingInfo;
