const paymentInfo = () => {
  const shippingMethod = {};
  const paymentInfoWrapper = document.querySelector('.paymentMethod');
  if (paymentInfoWrapper && window.location.search !== '') {
    let shippingMethodInfo = window.location.search.replace("?", "");
    let values = shippingMethodInfo.split('=');
    shippingMethod[values[0]] = values[1];

    let shippingMethodValue = shippingMethod.shippingMethod;
    if (shippingMethodValue === 'Standart') {
      shippingMethodValue = `${shippingMethodValue} Free`
    } else if (shippingMethodValue === 'Mail') {
      shippingMethodValue = `${shippingMethodValue} 139.91`
    } else if (shippingMethodValue === 'mailExpress') {
      shippingMethodValue = `${shippingMethodValue} $164.24`
    } else {
      shippingMethodValue = `${shippingMethodValue} $301.07`
    }
    localStorage.setItem("shippingMethod", shippingMethodValue);

    let numberInfoElement = document.querySelector('#contactInfo .customerInfo__value');
    let shipInfoElement = document.querySelector('#shipToInfo .customerInfo__value');
    let methodInfoElement = document.querySelector('#methodInfo .customerInfo__value');

    if (numberInfoElement) {
      numberInfoElement.innerHTML = localStorage.getItem('phoneOrMail');
    }

    if (shipInfoElement) {
      shipInfoElement.innerHTML = localStorage.getItem('shipAddress');
    }

    if (methodInfoElement) {
      methodInfoElement.innerHTML = shippingMethodValue || localStorage.getItem('shippingMethod');
    }
  }
};

export default paymentInfo;
