const checkedMethod = (event) => {
  const target = event.target;
  let shippingMethodValue = target.value;
  if (shippingMethodValue === 'Standart') {
    shippingMethodValue = `${shippingMethodValue} Free`
  } else if (shippingMethodValue === 'Mail') {
    shippingMethodValue = `${shippingMethodValue} 139.91`
  } else if (shippingMethodValue === 'mailExpress') {
    shippingMethodValue = `${shippingMethodValue} $164.24`
  } else {
    shippingMethodValue = `${shippingMethodValue} $301.07`
  }
  localStorage.setItem("shippingMethod", shippingMethodValue);
};

export default checkedMethod;
