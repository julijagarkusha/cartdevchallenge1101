const inputAddType = (event) => {
  const target = event.target;
  const targetParentElement = target.parentNode.parentNode;
  const flagsSwitch = targetParentElement.querySelector('.customSelect');
  if(target.value.charAt(0) !== "+") {
    target.type = 'mail';
    flagsSwitch.classList.value = 'customSelect customSelect--hidden';
  } else {
    target.type = 'text';
    flagsSwitch.classList.value = 'customSelect';
  }
};

export default inputAddType;
