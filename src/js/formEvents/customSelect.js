const customSelect = (event) => {
  const target = event.target;
  const optionList = target.nextElementSibling;
  const optionItems = optionList.childNodes;
  if(optionList.classList.value === "customSelect__options") {
    optionList.classList.value = "customSelect__options customSelect__options--visible";
  } else {
    optionList.classList.value = "customSelect__options";
  }
  optionItems.forEach(optionItem => {
    optionItem.addEventListener('click', optionSelected)
  });

};

const optionSelected = (event) => {
  const target = event.target;
  const customSelect = target.parentNode.parentNode;
  const defaultSelect = customSelect.querySelector('select');
  defaultSelect.value = target.getAttribute("data-value");
  const selectedOption = target.parentNode.previousElementSibling;
  selectedOption.classList.value = `customSelect__currentOption ${target.classList.value}`;

  if (customSelect.getAttribute('for') === 'country') {
    const selectValue = customSelect.querySelector('.customSelect__selectValue');
    selectValue.innerHTML = target.getAttribute('data-value');
    localStorage.setItem('country', target.getAttribute('data-value'))
  } else if (customSelect.getAttribute('for') === 'countryCode') {
    const parentInputWrapper = target.parentNode.parentNode.parentNode;
    const  parentInput = parentInputWrapper.querySelector('.customInput__input');
    parentInput.value = target.getAttribute('data-value');
  }

};
export default customSelect;
