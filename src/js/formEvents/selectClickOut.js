const selectClickOut = (event) => {
  const target = event.target;
  if (target.getAttribute("role") !== 'listbox') {
    const selectOpen = document.querySelectorAll(".customSelect__options");
    if (selectOpen) {
      selectOpen.forEach(selectItem => {
        selectItem.classList.value = "customSelect__options"
      })
    }
  }
};

export default selectClickOut;
