import LIVR from 'livr';
LIVR.Validator.defaultAutoTrim(true);

const inputOnFocus = (event) => {
  const target = event.target;
  if(target.value !== '') {
    target.classList.add('customInput__placeholder--visible');
  } else {
    target.classList.remove('customInput__placeholder--visible');
  }
    if(target.getAttribute('name') === 'EmailOrMobile' && target.getAttribute('type') === 'mail') {
      const inputWrapper = target.parentNode.parentNode;
      const errorMessageElement = inputWrapper.querySelector('.customInput__error');
      const validator = new LIVR.Validator({
        email:     [ 'required', 'email' ]
      });
      const validData = validator.validate({email: target.value});
      if (validData) {
        errorMessageElement.classList.value = 'customInput__error';
        localStorage.setItem('phoneOrMail', validData.email);
      } else {
        errorMessageElement.classList.value = 'customInput__error customInput__error--visible';
        errorMessageElement.innerHTML = 'Enter a valid email';
        localStorage.setItem('phoneOrMail', 'not valid email');
      }
    } else {
      const targetValue = target.value.replace(/(\d)(?= \d) /g, '$1');
      const inputWrapper = target.parentNode.parentNode;
      const errorMessageElement = inputWrapper.querySelector('.customInput__error');
      const validator = new LIVR.Validator({
        phone:     [ 'required', 'positive_integer', { max_length : 13 } ]
      });
      const validData = validator.validate({phone: targetValue});
      if (validData) {
        if (errorMessageElement) {
          errorMessageElement.classList.value = 'customInput__error';
          localStorage.setItem('phoneOrMail', validData.phone);
        }
      } else {
        if (errorMessageElement) {
          errorMessageElement.classList.value = 'customInput__error customInput__error--visible';
          errorMessageElement.innerHTML = 'Please enter a valid phone number (e.g. +1111111111)';
          localStorage.setItem('phoneOrMail', 'not valid phone');
        }
      }
    }
};

export default inputOnFocus;
