export default [
  {
    name: 'Dagacci Medical Uniform Womens Medical Scrub Set Top and Pant',
    type: 'Honeywell',
    vendor: 'InoMed',
    weight: '0.0',
    size: '',
    price: '325'
  },
  {
    name: 'Premier Short Sleeve Poplin Blouse Plain Works Shirt',
    type: 'Wolverine',
    vendor: 'InoMed',
    weight: '4.5',
    size: '',
    price: '90'
  },
  {
    name: '',
    type: 'Wolverine',
    vendor: 'InoMed',
    weight: '4.5',
    size: '',
    price: '90'
  },
  {
    name: 'Premier Pocket Tabard',
    type: 'Riggs Workwear',
    vendor: 'InoMed',
    weight: '0.7',
    size: 'L',
    price: '133'
  }
]
